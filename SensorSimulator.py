#/usr/bin/python3
#===================================================================#
#                          Sensor Simulator                         #
#===================================================================#
# This script generates a set of TCP and UDP servers that simulates #
# a set of sensors.                                                 #
#===================================================================#

import sys
import random
import socket
from time import sleep
import time
import _thread
import errno



# This function generates an string with n floats with a certain precision separated
# tokenSeparator
def genericResponse(nfields, tokenSeparator, precision):   
    response =  str(float(round(random.uniform(10,100),precision)))
    i = 0
    while i < ( nfields - 1 ) :
        i += 1
        response +=  tokenSeparator +  str(float(round(random.uniform(10,100),precision)))
    return response 
    

# 20.1250,  0.01029,   -0.310,   0.0570,1482.803, 10 Nov 2016, 12:37:08
def sbe37response():
    r = genericResponse(5, ', ',  5) +  ', 10 Nov 2016, 12:37:38\r\n'     
    return r

# SBE 54 Response
def sbe54response():
    return '''<Sample Num='684549' Type='Pressure'>
<Time>2016-11-15T00:04:10</Time>
<PressurePSI>''' + str(float(round(random.uniform(10,100),4)))+ '''</PressurePSI>
<PTemp>''' + str(float(round(random.uniform(10,100),4))) + '''</PTemp>
</Sample>''' + '\r\n'


# Teledyne Workhorse ADCP response
def ADCPresponse():
    bins = 20
    header = '2016-05-17' + ',' + '13:45:23' + ',' + genericResponse(4, ',', 2) 
    response = header
    for i in range(bins) :
        response += ',\r\n' + str(i + 1 ) + ',' + genericResponse(4, ',', 3) 
    return response


# 99/99/99    99:99:99    700    86    536
def econtuResponse():
    # OK
    resp = '99/99/99\t99:99:99:\t'
    resp +=  "%d" % random.uniform(100,999) + '\t' "%d" % random.uniform(100,999)+'\t' +"%d" % random.uniform(100,999) +'\r\n' 
    return resp

# Aanderaa 4831 response
def AanderaaResponse():
    resp = '4381\t606\t'
    resp += genericResponse(10, '\t', 3) + '\r\n'
    return resp

# EGIM Node response
def EGIMresponse():
    return genericResponse(25, ' ', 2) + '\r\n'


# Define a class that hots all the sensor  responses implementations
class Sensors():
    def __init__(self):
        self.names=[]
        self.functions=[]
        self.delays=[]
        self.ports=[]
        self.protocols=[]
        self.name=''
        
        # IP is server client in TCP mode and client IP in UDP mode
        self.IP='127.0.0.1' # default IP is localhost
        self.clientIP=''
        pass
    
    def msg(self, *args) :
        print(self.name, "==>", *args)
    
    # Change IP value
    def setIP(self, inIP):
        self.IP=inIP
    
    def addSensor(self, name, handler, delay, protocol, port):
        self.names.append(name)
        self.functions.append(handler)
        self.delays.append(delay)
        self.ports.append(port)
        self.protocols.append(protocol)
    
    def genResponse(self, inName):
        i = 0
        for n in self.names :
            self.msg("iteration", i, "inName", inName, "comparing with", n)
            if inName == n :
                return self.functions[i]()
            i+=1
        self.msg("error, sensors not found:", inName)
        
    def startSensor(self, name):
        self.setSensor(name)    

        if self.protocol == 'TCP' :
            self.startTCPserver(self.IP)
            
        elif self.protocol == 'UDP' :
            self.startUDPserver()
            
        else :
            self.msg("unknown protocol:", self.protocol)
            exit()
    def setClientIP(self, ip):
        self.clientIP=ip
    
    def startUDPserver(self):
        print("starting UDP server")
        # open connection

        #self.IP='147.83.159.150'
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) # force close when ctrl + C
    
        client_address = (self.clientIP, self.port)                
    
        while True :
            
            resp = self.response()            
            self.msg("Sending:")
            self.msg(resp)            
            sock.sendto(resp.encode(), client_address)                            
            sleep(self.delay)
        pass
    
        
    def startTCPserver(self, IP):
        self.msg("starting TCP server")
        # open connection
        port = self.port
        
        
       
        while True : 
            # wait for connections
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) # force close when ctrl + C
            server_address = (IP, port)
            print (self.name, 'starting up on IP', IP, 'port', port)
            sock.bind(server_address)
            sock.listen(1)
            self.msg("awaiting connection...")                    
            connection, unused = sock.accept()
            sock.settimeout(0.1) # set a timeout of 100 ms to check if it is still alive
            totalbytes = 0        
            inittime = int(time.time()) - 1
            byterate = 0
            sleep(2)
            alive = True
            
            while alive == True :                                            
                try :
                    resp = self.response()
                    self.msg(resp)
                    connection.sendall(resp.encode())
                    nbytes = len(resp.encode())
                    print(self.name, "sent ", nbytes, "bytes")
                    totalbytes += nbytes                                
                    sleep(self.delay)
                    
                except :
                    self.msg("Connection died, cleaning up...")                    
                    connection.close() 
                    alive = False                                            
                
            


    def setSensor(self, name):
        self.sensor = name                
        i = 0
        for n in self.names :
            
            if name == n :
                self.name = name
                self.response = self.functions[i] 
                self.delay = self.delays[i]
                self.port = self.ports[i]
                self.protocol = self.protocols[i]

                return True
            i += 1
        
        self.msg("Error, sensor not found:", name)
        self.msg("current sensors:")
        for n in self.names : 
            self.msg("    ->",n)


# Setup a thread server
def setupThread(name):
    sensors = Sensors() # generate new sensors
    sensors.setClientIP(clientIP) # add client IP
    sensors.addSensor('Workhorse', ADCPresponse, 60, 'TCP', 28011)
    sensors.addSensor('ECO_NTU', econtuResponse, 1, 'TCP', 28012)
    sensors.addSensor('Aanderaa', AanderaaResponse, 1, 'TCP',  28013)
    sensors.addSensor('SBE37', sbe37response, 10, 'TCP', 28014)
    sensors.addSensor('SBE54', sbe54response, 1, 'TCP', 28015)
    sensors.addSensor('EGIM', EGIMresponse, 20, 'UDP', 28016)  
    sensors.startSensor(name)  

#-------------------------------------------------------------------#
#-------------------------- START PROGRAM --------------------------#
#-------------------------------------------------------------------#
allSensors = ('Workhorse', 'ECO_NTU', 'Aanderaa', 'SBE37', 'SBE54', 'EGIM')
clientIP = '127.0.0.1'

if len(sys.argv) < 2 or len(sys.argv) > 3 :
    print("Wrong arguments")
    print("usage:", sys.argv[0], "<sensor>", "<client IP (optional)>")
    print("Available senors:")
    
    for sen in allSensors : 
        print("    ", sen)

    print("To start all sensors at once use \"all\"")
    print("Client IP is only used in UDP. Its default value is 127.0.0.1")
    exit()



if len(sys.argv) == 3 : 
    clientIP = sys.argv[2]
    print("Setting client IP to", clientIP)


if sys.argv[1] == 'all' :
    print("launch a server for each sensor")
    for name in allSensors :
        print("=============================>Launching server", name)
        _thread.start_new_thread (setupThread, (name,))
else :
    if sys.argv[1] in allSensors : 
        # The argument corresponds to an implemented sensor
        _thread.start_new_thread (setupThread, (sys.argv[1],))
    else :
        # sensor name not valid
        print("ERROR - Sensor not implemented:", sys.argv[1] )
        print("Available senors:")    
        for sen in allSensors : 
            print("    ", sen)
        exit()
     

 
# Infinite loop
while 1 : 
    sleep(10)




